Rails.application.routes.draw do

  root 'static_pages#home'
  get  'help' => 'static_pages#help'
  get  'about' => 'static_pages#about'
  get  'contact' => 'static_pages#contact'
  get  'signup' => 'users#new'
  get  'rate' => 'ratings#new'
  get  'add_beer' => 'beers#new'
  get  'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  get  'new_analysis' => 'analysis#new'
  get  'show_analysis' => 'analysis#show'
  post  'show_analysis' => 'analysis#show'
  resources :users
  resources :ratings
  resources :beers do
    collection { post:import}
  end
end
