require 'test_helper'

class RatingsControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:michael)
  end
  
  test "should get new when logged in" do
    log_in_as(@user)
    get :new
    assert_response :success
  end
  
  test "should redirect new when not logged in" do
    get :new
    assert_redirected_to login_url
  end

end
