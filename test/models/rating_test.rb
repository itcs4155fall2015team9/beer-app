require 'test_helper'

class RatingTest < ActiveSupport::TestCase
  
  def setup
    @rating = Rating.new(user_id: 1, beer_id: 2, rating1: 50, rating2: 50, rating3: 50)
  end

  test "should be valid" do
    assert @rating.valid?
  end
  
  test "user should be present" do
    @rating.user_id = nil
    assert_not @rating.valid?
  end
  
  test "beer should be present" do
    @rating.beer_id = nil
    assert_not @rating.valid?
  end
  
  test "rating should be between 0 - 100" do
    @rating.rating2 = 101
    assert_not @rating.valid?
  end
end
