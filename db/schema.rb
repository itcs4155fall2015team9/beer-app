# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151130005553) do

  create_table "beers", force: :cascade do |t|
    t.string   "name"
    t.string   "style"
    t.decimal  "original_gravity"
    t.decimal  "final_gravity"
    t.decimal  "abv"
    t.decimal  "ibu"
    t.decimal  "srm"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "image_path"
    t.decimal  "rating1_avg"
    t.decimal  "rating2_avg"
    t.decimal  "rating3_avg"
    t.integer  "rating_count",     default: 0
    t.decimal  "total_rating_avg"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "rating1"
    t.integer  "rating2"
    t.integer  "rating3"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "beer_id"
    t.integer  "total_rating"
  end

  add_index "ratings", ["user_id", "beer_id"], name: "index_ratings_on_user_id_and_beer_id", unique: true
  add_index "ratings", ["user_id"], name: "index_ratings_on_user_id"

  create_table "recommenders", force: :cascade do |t|
    t.boolean  "has_rated",       default: false
    t.decimal  "beer_percentage"
    t.integer  "user_id"
    t.integer  "beer_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "recommenders", ["beer_id"], name: "index_recommenders_on_beer_id"
  add_index "recommenders", ["user_id", "beer_id"], name: "index_recommenders_on_user_id_and_beer_id", unique: true
  add_index "recommenders", ["user_id"], name: "index_recommenders_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",            default: false
    t.decimal  "ibu"
    t.decimal  "original_gravity"
    t.decimal  "final_gravity"
    t.decimal  "abv"
    t.decimal  "srm"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
