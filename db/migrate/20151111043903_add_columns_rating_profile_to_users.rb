class AddColumnsRatingProfileToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ibu, :decimal
    add_column :users, :original_gravity, :decimal
    add_column :users, :final_gravity, :decimal
    add_column :users, :abv, :decimal
    add_column :users, :srm, :decimal
  end
end
