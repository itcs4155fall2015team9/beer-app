class RenameColumnAvbToAbvOnBeers < ActiveRecord::Migration
  def change
    rename_column :beers, :avb, :abv
  end
end
