class AddAddlRatingsCounterToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :rating1_avg, :decimal
    add_column :beers, :rating2_avg, :decimal
    add_column :beers, :rating3_avg, :decimal
    add_column :beers, :rating_count, :integer, default: 0
  end
end
