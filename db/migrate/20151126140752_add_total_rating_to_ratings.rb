class AddTotalRatingToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :total_rating, :integer
  end
end
