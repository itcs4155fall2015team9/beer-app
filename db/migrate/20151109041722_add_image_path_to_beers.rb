class AddImagePathToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :image_path, :string
  end
end
