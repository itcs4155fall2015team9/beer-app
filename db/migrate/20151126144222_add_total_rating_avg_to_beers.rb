class AddTotalRatingAvgToBeers < ActiveRecord::Migration
  def change
    add_column :beers, :total_rating_avg, :decimal
  end
end
