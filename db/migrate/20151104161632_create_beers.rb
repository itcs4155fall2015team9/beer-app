class CreateBeers < ActiveRecord::Migration
  def change
    create_table :beers do |t|
      t.string :name
      t.string :style
      t.decimal :original_gravity
      t.decimal :final_gravity
      t.decimal :avb
      t.decimal :ibu
      t.decimal :srm

      t.timestamps null: false
    end
  end
end
