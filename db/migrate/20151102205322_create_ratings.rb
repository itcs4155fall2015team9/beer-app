class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :rating1
      t.integer :rating2
      t.integer :rating3
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
