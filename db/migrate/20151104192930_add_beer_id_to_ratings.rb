class AddBeerIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :beer_id, :integer
    add_index :ratings, [:user_id, :beer_id], unique: true
  end
end
