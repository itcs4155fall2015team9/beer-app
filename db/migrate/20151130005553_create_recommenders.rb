class CreateRecommenders < ActiveRecord::Migration
  def change
    create_table :recommenders do |t|
      t.boolean :has_rated, default: FALSE
      t.decimal :beer_percentage
      t.references :user, index: true, foreign_key: true
      t.references :beer, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :recommenders, [:user_id, :beer_id], unique: true
  end
end
