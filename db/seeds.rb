User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: false)

User.create!(name:  "Tristan Williams",
             email: "triswill227@gmail.com",
             password:              "password",
             password_confirmation: "password",
             admin: true)
             
User.create!(name:  "Jeremy Wagner",
             email: "xalten@gmail.com",
             password:              "password2015",
             password_confirmation: "password2015",
             admin: true)

User.create!(name:  "Jarvis Williams",
             email: "jwill469@uncc.edu",
             password:              "password",
             password_confirmation: "password",
             admin: true)

50.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

Beer.create!(name: "Sierra Nevada Pale Ale Clone",
             style: "American Pale Ale",
             original_gravity: 1.055,
             final_gravity: 1.013,
             abv: 5.58,
             ibu: 39.79,
             srm: 8)
             
Beer.create!(name: "Zombie Dust Clone - EXTRACT",
             style: "American IPA",
             original_gravity: 1.063,
             final_gravity: 1.018,
             abv: 5.91,
             ibu: 70,
             srm: 8.98)
             
Beer.create!(name: "Bells Two Hearted Clone",
             style: "American IPA",
             original_gravity: 1.073,
             final_gravity: 1.018,
             abv: 7.14,
             ibu: 63.62,
             srm: 8.1)
             
Beer.create!(name: "Cascanookitra",
             style: "American IPA",
             original_gravity: 1.071,
             final_gravity: 1.014,
             abv: 7.53,
             ibu: 75.55,
             srm: 11.22)
             
Beer.create!(name: "Russian Imperial Stout",
             style: "Russian Imperial Stout",
             original_gravity: 1.095,
             final_gravity: 1.023,
             abv: 9.45,
             ibu: 75.15,
             srm: 50)

Beer.create!(name: "Midnight Ryed",
             style: "Imperial IPA",
             original_gravity: 1.085,
             final_gravity: 1.024,
             abv: 8.05,
             ibu: 166.82,
             srm: 40)
             
Beer.create!(name: "Tart Cherry Saison",
             style: "Belgian Specialty Ale",
             original_gravity: 1.042,
             final_gravity: 1.004,
             abv: 4.96,
             ibu: 26.03,
             srm: 4.48)
             
Beer.create!(name: "Kama Citra",
             style: "American IPA",
             original_gravity: 1.045,
             final_gravity: 1.012,
             abv: 4.4,
             ibu: 70.66,
             srm: 7.35)
             
Beer.create!(name: "Rogue Irish Clone",
             style: "Dortmunder Export",
             original_gravity: 1.051,
             final_gravity: 1.011,
             abv: 5.18,
             ibu: 8.33,
             srm: 5.32)
             
Beer.create!(name: "Phat Tyre Kit",
             style: "Oktoberfest/Marzen",
             original_gravity: 1.052,
             final_gravity: 1.01,
             abv: 5.48,
             ibu: 30.24,
             srm: 10.83)

Beer.all.each do |b|
  User.all.each do |u|
    Recommender.create!(user_id: u.id, beer_id: b.id)
  end
end


