class Beer < ActiveRecord::Base
  
  def self.import(file)
    beer_import_array = []
    CSV.foreach(file.path, headers: true) do |row|
      @beer = Beer.create!(row.to_hash)
      beer_import_array << @beer.id
    end
    beer_import_array
  end
  
  has_many :ratings, dependent: :destroy
  has_many :recommenders, dependent: :destroy
  mount_uploader :image_path, BeerImageUploader
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :style, presence: true
  validates :original_gravity, presence: true
  validates :final_gravity, presence: true
  validates :abv, presence: true
  validates :ibu, presence: true
  validates :srm, presence: true

  def self.search(search, hide_rated, user_id)
    
    if !search.blank? && hide_rated
      where("name LIKE ? AND recommenders.has_rated = ? AND recommenders.user_id = ?", "%#{search}%", FALSE, user_id)
    elsif !search.blank? && !hide_rated
      where("name LIKE ? AND recommenders.user_id = ?", "%#{search}%", user_id)
    elsif search.blank? && hide_rated
      where("recommenders.has_rated = ? AND recommenders.user_id = ?", FALSE, user_id)
    else
      where("recommenders.user_id = ?", user_id)
    end
  end
end

