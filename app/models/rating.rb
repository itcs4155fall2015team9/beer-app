class Rating < ActiveRecord::Base
  belongs_to :user
  belongs_to :beer
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :beer_id, presence: true
  validates :user_id, uniqueness: { scope: :beer_id, message: "you have already rated this beer!" }
  validates_inclusion_of :rating1, :rating2, :rating3, :in => 0..100
end
