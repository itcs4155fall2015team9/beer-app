$(function() {
    $("#slider").slider({
    	value: 50,
    	min: 0,
    	max: 100,
    	step: 1,
    	slide: function(event, ui) {
        	$("#rate1").val(ui.value);
    	}
	});
	$("#rate1").val($("#slider").slider("value"));
})
$(function() {
    $("#slider2").slider({
    	value: 50,
    	min: 0,
    	max: 100,
    	step: 1,
    	slide: function(event, ui) {
        	$("#rate2").val(ui.value);
    	}
	});
	$("#rate2").val($("#slider2").slider("value"));
})
$(function() {
    $("#slider3").slider({
    	value: 50,
    	min: 0,
    	max: 100,
    	step: 1,
    	slide: function(event, ui) {
        	$("#rate3").val(ui.value);
    	}
	});
	$("#rate3").val($("#slider3").slider("value"));
})