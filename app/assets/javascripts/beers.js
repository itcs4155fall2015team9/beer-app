$(function() {
  $("#beers").on('click', '.pagination a', function() {
    $.getScript(this.href);
    return false;
  });
  $("#beers_search input[type='checkbox']").change(function() {
    $.get($("beers_search").attr("action"), $("#beers_search").serialize(), null, "script");
    return false;
  });
  var options = {
    callback: function() {
      $.get($("beers_search").attr("action"), $("#beers_search").serialize(), null, "script");
    },
    wait: 750,
    highlight: false,
    captureLength: 1
  }
  
  $("#beers_search input").typeWatch( options );
  // **** Old way of doing it, keeping it in case the typeWatch has any bugs ****
  //$("#beers_search input").keyup(function() {
  //  $.get($("beers_search").attr("action"), $("#beers_search").serialize(), null, "script");
  //  return false;
  //});
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});