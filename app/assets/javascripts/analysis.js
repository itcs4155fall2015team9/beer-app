$(function () {
    $('#graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: gon.title
        },
        subtitle: {
            text: gon.subtitle
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: gon.y_title
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: gon.tooltip
        },
        series: [{
            name: 'User',
            data: gon.data,
            dataLabels: {
                enabled: true,
                color: '#000000',
                align: 'center',
                format: '{point.y}', // one decimal
                y: -5, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
    $('#xy').highcharts({
        chart: {
            zoomType: 'xy'
        },
        colors: ['#434348', '#7cb5ec'],
        title: {
            text: gon.title
        },
        subtitle: {
            text: gon.subtitle
        },
        xAxis: [{
            categories: gon.xdata,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            title: {
                text: gon.y1title,
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: gon.y2title,
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            min: 0,
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 75,
            verticalAlign: 'top',
            y: 85,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: gon.y2name,
            type: 'spline',
            yAxis: 1,
            zIndex: 2,
            data: gon.y2data

        }, {
            name: gon.y1name,
            type: 'column',
            zIndex: 1,
            data: gon.y1data
        }],
    });
});