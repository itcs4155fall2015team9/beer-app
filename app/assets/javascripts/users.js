$(document).ready(function() {
  
  var owl = $("#recommender-carousel");
  
  owl.owlCarousel({
    items : 5,
    itemsDesktop: [1000, 5],
    itemsDesktopSmall : [900, 3],
    itemsTablet : [600,2],
    itemsMobile : false
  });
  
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  })
});