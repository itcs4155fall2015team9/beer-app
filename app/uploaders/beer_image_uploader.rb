# encoding: utf-8

class BeerImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "assets/beer_image/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
   def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
      if !version_name
        return ActionController::Base.helpers.asset_path("fallback/default_beer.png")
      else
        return ActionController::Base.helpers.asset_path("fallback/default_beer_small.png")
      end
  #
  #   "/assets/fallback/" + [version_name, "default_beer.png"].compact.join('_')
  #   [version_name, "default_beer.png"].compact.join('_')
  #   [recommend, "default_beer_small.png"].compact.join('_')
   end

  # Process files as they are uploaded:
   process :resize_to_limit => [350, 350]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :recommend do
    process :resize_to_limit => [100, 100]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
