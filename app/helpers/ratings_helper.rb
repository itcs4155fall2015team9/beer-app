module RatingsHelper
  
  def update_recommend_matrix_percentages
    if current_user.ratings.count > 2
      Recommender.where(user_id: current_user.id).each do |i|
        i.beer_percentage = beer_percentage(current_user, i.beer_id)
        i.save
      end
    end
  end
  
  def update_recommend_matrix_new_user(user_id)
    Beer.all.each do |i|
      i.recommenders.create(user_id: user_id)
    end
  end
  
  def update_recommend_matrix_new_beer(beer_id)
    User.all.each do |i|
      @new_recommend = i.recommenders.new(beer_id: beer_id)
      if (i.abv? && i.ibu? && i.srm? && i.original_gravity? && i.final_gravity?)
        @new_recommend.beer_percentage = beer_percentage(i, beer_id)
      end
      @new_recommend.save
    end
  end
  
  def beer_percentage(user, beer_id)
    @beer = Beer.find(beer_id)
    ibu_per = get_percentage(user.ibu, @beer.ibu)
    original_gravity_per = get_percentage(user.original_gravity, @beer.original_gravity)
    final_gravity_per = get_percentage(user.final_gravity, @beer.final_gravity)
    abv_per = get_percentage(user.abv, @beer.abv)
    srm_per = get_percentage(user.srm, @beer.srm)
    final_percent = (ibu_per + original_gravity_per + final_gravity_per + abv_per + srm_per)/5
    final_percent
  end
  
  def get_percentage(user_val, beer_val)
    temp_percent = (1 - (user_val - beer_val).abs/user_val) * 100
    if temp_percent < 0
      return 0
    else
      return temp_percent
    end
  end
end
