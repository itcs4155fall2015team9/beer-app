class RatingsController < ApplicationController
  before_action :logged_in_user, only: [ :create, :new ]
  before_action :admin_user, only: :index
  before_action :destroy_rating, only: :destroy
  
  
  def new
    @rating = Rating.new
    @beer = Beer.find(params[:beer_id])
  end
  
  def index
    @ratings = Rating.paginate(page: params[:page])
  end
  
  def create
    @rating = Rating.new(rating_params)
    @rating.user_id = current_user.id
    @rating.total_rating = @rating.rating1 + @rating.rating2 + @rating.rating3
    if @rating.save
      flash[:success] = "Rating Submitted"
      generate_beer_profile
      update_beer_ratings
      update_recommend_matrix_percentages
      redirect_to current_user
    else
      render 'new'
    end
  end
  
  def destroy
    @rating.destroy
    Recommender.where(user_id: current_user.id).where(beer_id: @rating.beer_id).limit(1).update_all(has_rated: FALSE)
    flash[:success] = "Rating deleted"
    redirect_to current_user || root_url
  end
  
  private
  
    def rating_params
      params.require(:rating).permit(:rating1, :rating2, :rating3, :beer_id)
    end
    
    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    # Confirms correct user
    def correct_user
      @rating = current_user.ratings.find_by(id: params[:id])
      redirect_to root_url if @rating.nil?
    end
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
    
    def destroy_rating
      @rating = Rating.find(params[:id])
      if (!current_user.admin? && (@rating.user_id != current_user.id))
        redirect_to root_url
      end
    end
    
  require 'matrix'

  def regress x, y, degree
    x_data = x.map { |xi| (0..degree).map { |pow| (xi**pow).to_f } }
   
    mx = Matrix[*x_data]
    my = Matrix.column_vector(y)
   
    ((mx.t * mx).inv * mx.t * my).transpose.to_a[0]
  end
  
  def populate_arrays
    ibu = []
    original_gravity = []
    final_gravity = []
    abv = []
    srm = []
    rating = []
    
    rating_que = Rating.where(user_id: current_user.id)
    
    rating_que.each do |i|
      rating_temp = i.rating1 + i.rating2 + i.rating3
      beer = Beer.find(i.beer_id)
      rating << rating_temp
      ibu << beer.ibu
      original_gravity << beer.original_gravity
      final_gravity << beer.final_gravity
      abv << beer.abv
      srm << beer.srm
    end
    return rating, ibu, original_gravity, final_gravity, abv, srm
  end
  
  def generate_beer_profile
    
    rating, ibu, original_gravity, final_gravity, abv, srm = populate_arrays
    
    if rating.count < 3
      return
    end
    
    coefficients = regress ibu, rating, 2
    current_user.ibu = calc_best(coefficients)
    coefficients.clear
    
    coefficients = regress original_gravity, rating, 2
    current_user.original_gravity = calc_best(coefficients)
    coefficients.clear
    
    coefficients = regress final_gravity, rating, 2
    current_user.final_gravity = calc_best(coefficients)
    coefficients.clear
    
    coefficients = regress abv, rating, 2
    current_user.abv = calc_best(coefficients)
    coefficients.clear
    
    coefficients = regress srm, rating, 2
    current_user.srm = calc_best(coefficients)
    coefficients.clear
    
    current_user.save
  end
  
  def calc_best(coef)
    h = (-1 * coef.second)/(2 * coef.last)
    h
  end
  
  def update_beer_ratings
    @beer = Beer.find(@rating.beer_id)
    @beer.rating_count += 1
    @beer.rating1_avg = rolling_avg(@beer.rating1_avg, @beer.rating_count, @rating.rating1)
    @beer.rating2_avg = rolling_avg(@beer.rating2_avg, @beer.rating_count, @rating.rating2)
    @beer.rating3_avg = rolling_avg(@beer.rating3_avg, @beer.rating_count, @rating.rating3)
    @beer.total_rating_avg = @beer.rating1_avg + @beer.rating2_avg + @beer.rating3_avg
    @beer.save
    Recommender.where(user_id: current_user.id).where(beer_id: @beer.id).limit(1).update_all(has_rated: TRUE)
  end
  
  def rolling_avg(avg, count, sample)
    avg ||= 0
    avg -= avg/count
    avg += sample/count
    avg
  end
end
