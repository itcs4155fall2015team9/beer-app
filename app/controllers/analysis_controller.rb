class AnalysisController < ApplicationController
  before_action :admin_user,     only: [:show, :new]
  
  def new
  end

  def show
    gon.data = []
    gon.xdata = []
    gon.y1data = []
    gon.y2data = []
    case params[:choice]
    when "best_beer"
      @chart_type = 'graph'
      gon.title = "Best Overall Rated Beers"
      gon.subtitle = "Top 10 beers rated from best to worst based on average rating"
      gon.y_title = "Rating Average (300 max)"
      gon.tooltip  = 'Rating Average: <b>{point.y}</b>'
      data_set = Beer.all.order(total_rating_avg: :desc).where.not(total_rating_avg: nil).limit(10)
      data_set.each do |i|
        gon.data << [i.name, i.total_rating_avg.to_f]
      end
    when "worst_beer"
      @chart_type = 'graph'
      gon.title = "Worst Overall Rated Beers"
      gon.subtitle = "Lowest 10 beers rated from best to worst based on average rating"
      gon.y_title = "Rating Average (300 max)"
      gon.tooltip  = 'Rating Average: <b>{point.y}</b>'
      data_set = Beer.all.order(total_rating_avg: :asc).where.not(total_rating_avg: nil).limit(10)
      data_set.each do |i|
        gon.data << [i.name, i.total_rating_avg.to_f]
      end
    when "most_rated_beer"
      @chart_type = 'graph'
      gon.title = "Most Rated Beers"
      gon.subtitle = "Top 10 most rated beers from most to least"
      gon.y_title = "Total Number of Ratings"
      gon.tooltip = 'Number of times rated: <b>{point.y}</b>'
      data_set = Beer.all.order(rating_count: :desc).where("rating_count > ?", 0).limit(10)
      data_set.each do |i|
        gon.data << [i.name, i.rating_count]
      end
    when "best_rated_1_beer"
      @chart_type = 'graph'
      gon.title = "Beers With the Best First Impression"
      gon.subtitle = "Top 10 most rated beers visually and for their aroma based on average rating"
      gon.y_title = "Rating Average (100 max)"
      gon.tooltip = 'Total Rating Avg: <b>{point.y}</b>'
      data_set = Beer.all.order(rating1_avg: :desc).where.not(rating1_avg: nil).limit(10)
      data_set.each do |i|
        gon.data << [i.name, i.rating1_avg.to_f]
      end
    when "best_rated_2_beer"
      @chart_type = 'graph'
      gon.title = "Beers With the Best Taste Rating"
      gon.subtitle = "Top 10 most rated beers for their taste based on average rating"
      gon.y_title = "Rating Average (100 max)"
      gon.tooltip = 'Average Rating: <b>{point.y}</b>'
      data_set = Beer.all.order(rating2_avg: :desc).where.not(rating2_avg: nil).limit(10)
      data_set.each do |i|
        gon.data << [i.name, i.rating1_avg.to_f]
      end
    when "best_rated_3_beer"
      @chart_type = 'graph'
      gon.title = "Beers With the Best After-taste"
      gon.subtitle = "Top 10 most rated beers for their after-taste and fill based on average rating"
      gon.y_title = "Rating Average (100 max)"
      gon.tooltip = 'Average Rating: <b>{point.y}</b>'
      data_set = Beer.all.order(rating3_avg: :desc).where.not(rating3_avg: nil).limit(10)
      data_set.each do |i|
        gon.data << [i.name, i.rating3_avg.to_f]
      end
    when "combo_rated"
      @chart_type = 'xy'
      gon.title = "Overall Average Rating vs Times Rated"
      gon.subtitle = "Combination Graph of Average Rating vs Number of Times Rated ordered by best rating"
      gon.y1title = "Rating Average (300 max)"
      gon.y2title = "Number of times rated"
      gon.y1name = "Average Rating"
      gon.y2name = "Times Rated"
      data_set = Beer.all.order(total_rating_avg: :desc).where.not(total_rating_avg: nil).limit(10)
      data_set.each do |i|
        gon.xdata << [i.name]
        gon.y1data << [i.total_rating_avg.to_f]
        gon.y2data << [i.rating_count]
      end
    when "combo_rated_reversed"
      @chart_type = 'xy'
      gon.title = "Overall Average Rating vs Times Rated"
      gon.subtitle = "Combination Graph of Average Rating vs Number of Times Rated ordered by most rated"
      gon.y1title = "Rating Average (300 max)"
      gon.y2title = "Number of times rated"
      gon.y1name = "Average Rating"
      gon.y2name = "Times Rated"
      data_set = Beer.all.order(rating_count: :desc).where.not(rating_count: nil).limit(10)
      data_set.each do |i|
        gon.xdata << [i.name]
        gon.y1data << [i.total_rating_avg.to_f]
        gon.y2data << [i.rating_count]
      end
    end
  end
  
  private
  
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
