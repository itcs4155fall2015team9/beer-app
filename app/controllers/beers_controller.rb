class BeersController < ApplicationController
  before_action :logged_in_user, only: [:index,:show]
  before_action :admin_user,     only: [:destroy, :update]
  
  def new
    @beer = Beer.new
  end

  def import
    beer_import_array = Beer.import(params[:file])
    beer_import_array.each do |i|
      update_recommend_matrix_new_beer(i)
    end
    flash[:success] = "Beers added successfully"
    redirect_to beers_path
  end

  def show
    @beer = Beer.find(params[:id])
    unless current_user.ibu.nil? 
      @percentage = 0
    end
  end
  
  def edit
    @beer = Beer.find(params[:id])
  end
  
  def update
    @beer = Beer.find(params[:id])
    if @beer.update_attributes(beer_params)
      flash[:success] = "Beer updated"
      redirect_to @beer
    else
      render 'edit'
    end
  end
  
  def destroy
    Beer.find(params[:id]).destroy
    flash[:success] = "Beer deleted"
    redirect_to beers_path
  end

  def index
	    @beers = Beer.joins(:recommenders).search(params[:search], params[:hide_rated], current_user.id).order(name: :ASC).paginate(:per_page => 16, page: params[:page])
  end

  def create
    @beer = Beer.new(beer_params)
    if @beer.save
      update_recommend_matrix_new_beer(@beer.id)
      flash[:success] = "Beer successfully saved!"
      redirect_to @beer
    else

      render 'new'
    end
  end
  
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
  
    def beer_params
      params.require(:beer).permit(:name, :style, :original_gravity, :final_gravity, :abv, :ibu, :srm, :image_path)
    end
end
